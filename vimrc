syntax on

set hidden
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
set smartindent
set nu
set laststatus=2
set termguicolors

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'dense-analysis/ale'
Plug 'ap/vim-buftabline'
Plug 'Donaldttt/fuzzyy'
Plug 'vim-airline/vim-airline'
Plug 'joshdick/onedark.vim'

" Initialize plugin system
call plug#end()

set omnifunc=ale#completion#OmniFunc
" Give more space for displaying messages.
set cmdheight=2

set guifont=Inconsolata\ Nerd\ Font\ Mono:h20

nnoremap <silent> <S-h> :bn<CR>
nnoremap <silent> <S-l> :bp<CR>
nnoremap <leader>b :ls<cr>:b<space>
nnoremap ]a :ALENextWrap<CR>
nnoremap [a :ALEPreviousWrap<CR>

let g:ale_completion_enabled = 1
"let g:ale_linters = {'c': ['clangtidy'], 'cpp': ['cc'], 'python': ['flake8'], 'go': ['gopls', 'golangci-lint'], 'zig': ['zls']}
"let g:ale_cpp_cc_options = '-std=c++20 -Wall -Wextra'
let g:airline_theme='onedark'
" Set this. Airline will handle the rest.
let g:airline#extensions#ale#enabled = 1

colorscheme onedark
